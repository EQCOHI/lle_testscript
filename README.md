# LLE_TestScript

Can test three LLE algorithm 

1. Zero-DCE, 2. RetinexNet, 3. ULE
-------------------------------------------------------------

# Requirements 
- conda install tensorflow-gpu=1.14 numpy=1.16
- conda install pytorch==1.5.0 torchvision==0.6.0 -c pytorch 
- conda install -c anaconda scikit-learn
- conda install -c anaconda scikit-image
- conda install -c conda-forge opencv
- conda install -c conda-forge tqdm 

-------------------------------------------------------------

# Dataset 
put the data set 

.data/

    ---part2

    ---vv

    ---part2_label
    
you can download data set here: https://drive.google.com/drive/folders/1_FfhSso6h0mi8bJKTv-CHjo9GsHdmX8x?usp=sharing 

-------------------------------------------------------------
# test 
python test_script.py --model ['zero-dce' or 'retinex' or 'ULE'] --testData [part2 or vv] --device # of device --num-visualize # of visualize(optional)

test result saved in ./script-output/[modelName]/images 

-------------------------------------------------------------

