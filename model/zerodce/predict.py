import os
import sys
# sys.path.append('/home/choieq/dabu/LLE/code')
sys.path.append('../../code')

from PIL import Image
import numpy as np

import torch
from torch.utils.data import DataLoader
from torchvision import transforms

from .zcemodel import *
from utils import *
from .dataset import *

import time
from tqdm import tqdm


def predict(testData, testDir, n_visualize, device, refDir):

    # ckpt = '/home/choieq/dabu/LLE/train-jobs/zero-dce/5LE-300_best_model.pth'
    ckpt = 'train-jobs/zero-dce/5LE-300_best_model.pth'

    checkpoint = torch.load(ckpt, map_location='cuda')
    hp = checkpoint['HyperParam']

    model = DCENet(n_LE=hp['n_LE'], std=hp['std'])
    model.load_state_dict(checkpoint['model'])
    model.to('cuda')
    model.eval()

    # Save directory for Visual comparison & Quantitative Result(PSNR, SSIM)
    output_dir = '/home/choieq/dabu/LLE/script-output/zerodce/{}/images'.format(testData)
    output_dir = 'script-output/Zero_DCE/{}/images'.format(testData)

    if torch.cuda.is_available() and device >= 0:
        device = torch.device(f'cuda:{device}')
        torch.cuda.manual_seed(1234)
    else:
        device = torch.device('cpu')
        torch.manual_seed(1234)

    outPath = create_dir(output_dir)

    test_dataset = SICEPart1(testDir, transform=transforms.ToTensor())
    test_loader = DataLoader(test_dataset, batch_size=1, shuffle=False)

    # for quantitative result
    avg_psnr = []
    avg_ssim = []
    f_acc = open(os.path.join(output_dir[:-7] + '/acc-' + 'zero-dce' + '.txt'), 'w')

    # for number of visual comparison
    num_visualize = 0

    # for write run time
    f_runtime = open(os.path.join(output_dir[:-7] + '/avg_runtime-' + 'zero-dce' + '.txt'), 'w')
    run_time = 0

    """ start evaluation! """
    with torch.no_grad():
        for i, sample in enumerate(tqdm(test_loader)):
            name = sample['name'][0][:-4]

            # first 3 low light images used for test
            # if int(name.split('_')[1]) > 3:
            #     continue

            img_batch = sample['img'].to(device)

            # Do test
            start = time.time()
            Astack = model(img_batch)
            enhanced_batch = refine_image(img_batch, Astack)
            end = time.time()
            run_time += end - start
            avg_run_time = run_time / (i + 1)

            img = to_numpy(img_batch, squeeze=True)
            enhanced = to_numpy(enhanced_batch, squeeze=True)

            # save original image
            origin_img_saved = Image.fromarray((img * 255).astype(np.uint8), mode='RGB')

            # save enhanced image
            output_img_saved = Image.fromarray((enhanced * 255).astype(np.uint8), mode='RGB')
            tmp = output_img_saved.convert('RGB')
            tmp = np.array(tmp)

            """ For Visual Comparision """
            if num_visualize < n_visualize:
                origin_img_saved.save(outPath.joinpath(f'input_{name}.png'))
                output_img_saved.save(outPath.joinpath(f'output_zerodce_{name}.png'))

            num_visualize += 1

            """ For Quantitative Result """
            if testData == 'part2':
                ref_img = get_ref_img(name, refDir)

                avg_psnr.append(get_psnr(tmp, ref_img))
                # print('name: {}'.format(name))

                avg_ssim.append(get_ssim(tmp, ref_img))

        """Only part2 has a reference image"""
        if testData == 'part2':
            f_acc.write('avg PSNR: {:.5f}, avg SSIM: {:.5f}'.format(sum(avg_psnr) / len(avg_psnr),
                                                                    sum(avg_ssim) / len(avg_ssim)))
        else:
            f_acc.write("No PSNR, SSIM. Only SICE part2 dataset can calculate PSNR, SSIM.")

        f_runtime.write("avg runtime: {}".format(str(avg_run_time) + 'sec'))
        f_acc.close()
        f_runtime.close()

