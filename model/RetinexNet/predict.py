from glob import glob
import sys
# sys.path.append('/home/choieq/dabu/model/RetinexNet')
sys.path.append('../../model/RetinexNet')


from model.RetinexNet.rtnmodel import *


def predict(testData, testDir, n_visualize, device, refDir):
    test_low_data_names = glob(testDir + '/' + '*.*')
    test_low_data_names.sort()
    print('Number of evaluation images: %d' % len(test_low_data_names))

    # output_dir = '/home/choieq/dabu/LLE/script-output/RetinexNet/{}/images'.format(testData)
    output_dir = 'script-output/RetinexNet/{}/images'.format(testData)
    print(os.getcwd())
    ckpt_dir = 'train-jobs/retinex/'

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)


    model = RetinexNet().cuda()

    model.predict(test_low_data_names, res_dir=output_dir, ckpt_dir=ckpt_dir
                  , n_visualize=n_visualize, refDir=refDir)

