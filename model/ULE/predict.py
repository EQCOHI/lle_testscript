from __future__ import print_function

import os


os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import argparse
from glob import glob

import tensorflow as tf
import torch
from .ule_model import *
# from utils import *


def lowlight_test(lowlight_enhance, testData, testDir, n_visualize, refDir):

    save_dir = 'script-output/ULE/{}/images'.format(testData)
    # save_dir = '/home/choieq/dabu/LLE/script-output/ULE/{}/images'.format(testData)
    # ckpt_dir = '/home/choieq/dabu/LLE/train-jobs/ULE/'
    ckpt_dir = 'train-jobs/ULE/'

    if not os.path.exists(save_dir):
        os.makedirs(save_dir)

    test_low_data_names = glob(os.path.join(testDir, '*.*'))

    print('[*] Number of test data: %d' % len(test_low_data_names))

    lowlight_enhance.test(test_low_data_names, ckpt_dir=os.path.join(ckpt_dir, 'Enhancement'), save_dir=save_dir, post_processing=False, denoising_level=1, n_visualize=n_visualize, refDir=refDir)


def predict(testData, testDir, n_visualize, device, refDir):

    if torch.cuda.is_available():
        use_gpu = True
    else:
        use_gpu = False

    if use_gpu:
        print("[*] GPU\n")
        os.environ["CUDA_VISIBLE_DEVICES"] = str(device) #args.gpu_idx
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True

        with tf.Session(config=config) as sess:

            model = lowlight_enhance(sess, is_training=False)
            lowlight_test(model, testData, testDir, n_visualize, refDir)

    else:
        print("[*] CPU\n")
        with tf.Session() as sess:

            model = lowlight_enhance(sess, is_training=False)
            lowlight_test(model, testData, testDir, n_visualize, refDir)
