import numpy as np
from PIL import Image
import cv2
import tensorflow as tf

tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)


def data_augmentation(image, mode):
    if mode == 0:
        # original
        return image
    elif mode == 1:
        # flip up and down
        return np.flipud(image)
    elif mode == 2:
        # rotate counterwise 90 degree
        return np.rot90(image)
    elif mode == 3:
        # rotate 90 degree and flip up and down
        image = np.rot90(image)
        return np.flipud(image)
    elif mode == 4:
        # rotate 180 degree
        return np.rot90(image, k=2)
    elif mode == 5:
        # rotate 180 degree and flip
        image = np.rot90(image, k=2)
        return np.flipud(image)
    elif mode == 6:
        # rotate 270 degree
        return np.rot90(image, k=3)
    elif mode == 7:
        # rotate 270 degree and flip
        image = np.rot90(image, k=3)
        return np.flipud(image)


def load_images(file):
    im = Image.open(file)
    im = im.resize((512,512))
    im = np.array(im, dtype="float32") / 255.0

    if im.shape[-1] == 4:
        im = cv2.cvtColor(im, cv2.COLOR_RGBA2RGB)

    return im


def save_images(filepath, result_1, result_2=None, result_3=None, result_4=None, cvt=False, suffix='png'):
    result_1 = np.squeeze(result_1)
    result_2 = np.squeeze(result_2)
    result_3 = np.squeeze(result_3)
    result_4 = np.squeeze(result_4)

    if not result_2.any():
        cat_image = result_1
    else:
        cat_image = np.concatenate([result_1, result_2], axis=1)

    if result_3.any():
        cat_image = np.concatenate([cat_image, result_3], axis=1)

    if result_4.any():
        cat_image = np.concatenate([cat_image, result_4], axis=1)

    if cvt:
        cat_image = cv2.cvtColor(cat_image, cv2.COLOR_HSV2RGB_FULL)

    im = Image.fromarray(np.clip(cat_image * 255.0, 0, 255.0).astype(np.uint8))

    im.save(filepath)


def extract_patches(im, ks):
    ks_h = int(ks / 2)
    indx = range(-ks_h, ks_h + 1)
    indy = range(-ks_h, ks_h + 1)

    for i in indx:
        for j in indy:
            patch = tf.roll(im, [i, j], axis=[1, 2])

            if i == (-ks_h) and j == (-ks_h):
                patches = patch
            else:
                patches = tf.concat([patches, patch], axis=3)

    return patches


def gradient(input_tensor, direction):
    smooth_kernel_x = tf.reshape(tf.constant([[0, 0], [-1, 1]], tf.float32), [2, 2, 1, 1])
    smooth_kernel_y = tf.transpose(smooth_kernel_x, [1, 0, 2, 3])

    if direction == "x":
        kernel = smooth_kernel_x
    elif direction == "y":
        kernel = smooth_kernel_y
    return tf.abs(tf.nn.conv2d(input_tensor, kernel, strides=[1, 1, 1, 1], padding='SAME'))


def ave_gradient( input_tensor, direction):
    return tf.layers.average_pooling2d(gradient(input_tensor, direction), pool_size=3, strides=1,
                                       padding='SAME')


def BrightChannel(im, sz):

    temp = extract_patches(im, sz)
    bright = tf.reduce_max(temp, axis=-1, keepdims=True)

    return bright


def AtmLight(im, bright):
    im_shape = tf.shape(im)
    b, h, w, c = im_shape[0], im_shape[1], im_shape[2], im_shape[3]
    imsz = h * w
    numpx = tf.to_int32(tf.maximum(tf.floor(imsz / 1000), 1))
    brightvec = tf.reshape(bright, [b, imsz, 1])
    imvec = tf.reshape(im, [b, imsz, 3])

    indices = tf.argsort(brightvec, axis=1, direction='DESCENDING')
    indices = indices[:, imsz - numpx::]

    f1 = tf.expand_dims(tf.reduce_sum(tf.gather_nd(imvec[0], indices[0]), axis=0, keepdims=True), axis=0)
    f2 = tf.map_fn(lambda x: tf.concat(
        [tf.expand_dims(tf.reduce_sum(tf.gather_nd(imvec[x], indices[x]), axis=0, keepdims=True), axis=0)], axis=0),
                   tf.range(b), dtype=tf.float32)

    atmsum = tf.cond(b > 1, lambda: f2, lambda: f1)
    atmsum = tf.reshape(atmsum, [b, 1, 3])

    A = atmsum / tf.to_float(numpx)

    return A


def initial_trans_map(im, A, ksize=3, omega=0.8):

    M = 0
    A_max = tf.expand_dims(tf.reduce_min(A, axis=[1, 2], keep_dims=True), axis=3)

    temp = (1 + M - BrightChannel(im, ksize)) / (1 + M - A_max)

    t_hat = 1 - omega * temp
    t_hat = tf.clip_by_value(t_hat, 0, 1)

    return t_hat


def Recover(im, t, A, th_low=0.1):

    t = tf.maximum(t, th_low)

    #input_img = tf.reshape(im, (tf.shape(im)[0], -1, 3))

    #input_sub_A_max = tf.reduce_max(input_img - A, axis=-1, keep_dims=True)
    #A_max = tf.reduce_max(A, axis=-1, keep_dims=True)

    #thr = input_sub_A_max / (1 - A_max)
    #thr = tf.reshape(thr, tf.shape(t))
    #t = tf.maximum(t, thr)
    t = tf.clip_by_value(t, 0, 1)

    A = tf.expand_dims(A, axis=1)

    res = (im - A) / t + A
    res = tf.clip_by_value(res, 0, 1)

    return res


def compute_W(im, ksize=3):
    patch_size = ksize * ksize
    eps = 1e-4

    patches = extract_patches(im, ksize)
    b = tf.shape(im)[0]
    h = tf.shape(im)[1]
    w = tf.shape(im)[2]
    c = tf.shape(im)[3]

    patches = tf.reshape(patches, [-1, ksize * ksize, c])
    mean = tf.reduce_mean(patches, axis=1, keepdims=True)
    cov = (tf.einsum('...ji,...jk->...ik', patches, patches) / patch_size) - tf.einsum('...ji,...jk->...ik',
                                                                                          mean, mean)
    U = tf.eye(3)

    inv = tf.linalg.inv(cov + (eps / patch_size) * U)

    diff = patches - tf.tile(mean, [1, patch_size, 1])

    X = tf.einsum('...ij,...jk->...ik', diff, inv)
    W = (1.0 / patch_size) * (1 + tf.einsum('...ij,...jk->...ik', X, tf.transpose(diff, [0, 2, 1])))

    W = tf.reshape(W, [b, h, w, patch_size * patch_size])

    return W
