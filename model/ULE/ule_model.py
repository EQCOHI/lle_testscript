from __future__ import print_function

import os
import sys
sys.path.append('/home/choieq/dabu/LLE/model/ULE')
sys.path.append('../../code')
from utils import *

import time
import random
from .denoising_model import *

from ule_utils import *

import tensorflow as tf
import numpy as np
from PIL import Image
import time
from tqdm import tqdm

def Enhancement(input_L, attmap=None, channel=64, kernel_size=3):

    input_im = input_L

    activation = tf.nn.relu

    if attmap is None:
        attmap = tf.ones([tf.shape(input_im)[0], tf.shape(input_im)[1], tf.shape(input_im)[2], 1])

    attmap1 = tf.layers.average_pooling2d(attmap, 3, 2, padding='same')
    attmap2 = tf.layers.average_pooling2d(attmap1, 3, 2, padding='same')
    attmap3 = tf.layers.average_pooling2d(attmap2, 3, 2, padding='same')
    attmap4 = tf.layers.average_pooling2d(attmap3, 3, 2, padding='same')

    with tf.variable_scope('EnhanceNet'):
        conv0 = attmap * tf.layers.conv2d(input_im, channel, kernel_size, padding='same', activation=None)
        conv1 = attmap1 * tf.layers.conv2d(conv0, channel, kernel_size, strides=2, padding='same', activation=activation)
        conv2 = attmap2 * tf.layers.conv2d(conv1, channel, kernel_size, strides=2, padding='same', activation=activation)
        conv3 = attmap3 * tf.layers.conv2d(conv2, channel, kernel_size, strides=2, padding='same', activation=activation)
        conv4 = attmap4 * tf.layers.conv2d(conv3, channel, kernel_size, strides=2, padding='same', activation=activation)

        up1 = tf.image.resize_bilinear(conv4, (tf.shape(conv3)[1], tf.shape(conv3)[2]))
        deconv1 = attmap3 * tf.layers.conv2d(up1, channel, kernel_size, padding='same', activation=activation) + conv3
        up2 = tf.image.resize_bilinear(deconv1, (tf.shape(conv2)[1], tf.shape(conv2)[2]))
        deconv2 = attmap2 * tf.layers.conv2d(up2, channel, kernel_size, padding='same', activation=activation) + conv2
        up3 = tf.image.resize_bilinear(deconv2, (tf.shape(conv1)[1], tf.shape(conv1)[2]))
        deconv3 = attmap1 * tf.layers.conv2d(up3, channel, kernel_size, padding='same', activation=activation) + conv1
        up4 = tf.image.resize_bilinear(deconv3, (tf.shape(conv0)[1], tf.shape(conv0)[2]))
        deconv4 = attmap * tf.layers.conv2d(up4, channel, kernel_size, padding='same', activation=activation) + conv0

        deconv1_resize = tf.image.resize_bilinear(deconv1, (tf.shape(deconv4)[1], tf.shape(deconv4)[2]))
        deconv2_resize = tf.image.resize_bilinear(deconv2, (tf.shape(deconv4)[1], tf.shape(deconv4)[2]))
        deconv3_resize = tf.image.resize_bilinear(deconv3, (tf.shape(deconv4)[1], tf.shape(deconv4)[2]))
        feature_gather = tf.concat([deconv1_resize, deconv2_resize, deconv3_resize, deconv4], axis=-1)
        feature_fusion = attmap * tf.layers.conv2d(feature_gather, channel, 1, padding='same', activation=None)

        output = attmap * tf.layers.conv2d(feature_fusion, 1, 3, padding='same', activation=tf.nn.sigmoid)

        # output = tf.clip_by_value(output, 0, 256)

    return 1 - output


class lowlight_enhance(object):
    def __init__(self, sess, is_training=True):
        self.sess = sess
        self.DecomNet_layer_num = 4

        # build the model
        self.input_low = tf.placeholder(tf.float32, [None, None, None, 3], name='input_low')

        ks = 3

        input_hsv = tf.image.rgb_to_hsv(self.input_low)
        v = 1 - tf.split(input_hsv, 3, axis=-1)[-1]

        self.attmap = v ** 1.5
        #self.attmap = v ** (1.0 / v)

        self.bright = BrightChannel(self.input_low, 15)
        self.A = AtmLight(self.input_low, self.bright)

        self.t_hat = initial_trans_map(self.input_low, self.A, ksize=ks, omega=0.6)
        self.W = compute_W(self.input_low, ks)
        self.t = Enhancement(self.input_low, self.attmap)
        self.output_S_hat = Recover(self.input_low, self.t, self.A, 0)

        t_patch = extract_patches(self.t, ks)
        b = tf.shape(t_patch)[0]
        h = tf.shape(t_patch)[1]
        w = tf.shape(t_patch)[2]

        T_I = tf.tile(t_patch, [1, 1, 1, ks * ks])
        T_J = tf.reshape(tf.tile(tf.expand_dims(t_patch, axis=-1), [1, 1, 1, 1, ks * ks]), [b, h, w, ks ** 4])

        self.E1 = tf.reduce_mean(self.W * tf.losses.mean_squared_error(T_I, T_J))

        mean_t_hat = tf.reduce_mean(self.t_hat, axis=(1, 2, 3), keep_dims=True)
        mean_t = tf.reduce_mean(self.t, axis=(1, 2, 3), keep_dims=True)
        var_t_hat = tf.reduce_mean(tf.abs((self.t_hat - mean_t_hat) ** 2), axis=(1, 2, 3), keep_dims=True)
        var_t = tf.reduce_mean(tf.abs((self.t - mean_t) ** 2), axis=(1, 2, 3), keep_dims=True)

        self.E2 = 1 * tf.losses.mean_squared_error(mean_t_hat, mean_t) + tf.losses.mean_squared_error(var_t, var_t_hat)

        self.smoothness = tf.reduce_sum(tf.image.total_variation(self.t))

        self.saturation = self.saturation_loss(self.input_low, self.t, self.A, 1.0)

        #self.enhance_loss = 1 * self.E1 + 5.0 * self.E2 + 1e-4 * self.smoothness + 0.1 * self.saturation
        self.enhance_loss = 1 * self.E1 + 1.0 * self.E2 + 1e-2 * self.smoothness + 0.1 * self.saturation

        self.lr = tf.placeholder(tf.float32, name='learning_rate')
        optimizer = tf.train.AdamOptimizer(self.lr, name='AdamOptimizer')

        self.var_Enhance = [var for var in tf.trainable_variables() if 'EnhanceNet' in var.name]

        self.train_op_Enhance = optimizer.minimize(self.enhance_loss, var_list=self.var_Enhance)

        self.sess.run(tf.global_variables_initializer())

        self.saver = tf.train.Saver(var_list=self.var_Enhance)

        print("[*] Initialize model successfully...")

    def saturation_loss(self, input_img, t, A, gamma=1.0):

        input_img = tf.reshape(input_img, (tf.shape(input_img)[0], -1, 3))
        t = tf.reshape(t, (tf.shape(input_img)[0], -1, 1))

        thr = tf.reduce_max(input_img - A / (1 - A + 1e-6), axis=-1, keepdims=True)

        mask = tf.cast(t <= (thr ** (1 / gamma)), dtype=tf.float32)
        diff = (t - thr) * mask

        return tf.reduce_mean((diff ** 2) / 2)

    def smooth(self, input_I, input_R):
        input_R = tf.image.rgb_to_grayscale(input_R)
        return tf.reduce_mean(
            gradient(input_I, "x") * tf.exp(-1 * ave_gradient(input_R, "x")) + gradient(input_I,
                                                                                        "y") * tf.exp(
                -1 * ave_gradient(input_R, "y")))

    def evaluate(self, epoch_num, eval_low_data_names, sample_dir):
        print("[*] Evaluating for epoch %d..." % (epoch_num))

        for idx, eval_low_data_name in enumerate(eval_low_data_names):
            eval_low_data = load_images(eval_low_data_name)
            input_low_eval = np.expand_dims(eval_low_data, axis=0)

            t_hat, t, low, S_hat = self.sess.run(
                [self.t_hat, self.t, self.input_low, self.output_S_hat],
                feed_dict={self.input_low: input_low_eval})
            t_hat = np.concatenate([t_hat, t_hat, t_hat], axis=3)
            t = np.concatenate([t, t, t], axis=3)

            save_images(os.path.join(sample_dir, 'eval_%d_%d.png' % (idx + 1, epoch_num)), t_hat, t,
                        low, S_hat)

    def save(self, saver, iter_num, ckpt_dir, model_name):
        if not os.path.exists(ckpt_dir):
            os.makedirs(ckpt_dir)
        print("[*] Saving model %s" % model_name)
        saver.save(self.sess, os.path.join(ckpt_dir, model_name), global_step=iter_num)

    def load(self, saver, ckpt_dir):
        ckpt = tf.train.get_checkpoint_state(ckpt_dir)
        if ckpt and ckpt.model_checkpoint_path:
            full_path = tf.train.latest_checkpoint(ckpt_dir)
            try:
                global_step = int(full_path.split('/')[-1].split('-')[-1])
            except ValueError:
                global_step = None
            saver.restore(self.sess, full_path)
            return True, global_step
        else:
            print("[*] Failed to load model from %s" % ckpt_dir)
            return False, 0

    def test(self, test_low_data_names, ckpt_dir, save_dir, post_processing=False, denoising_level=1, n_visualize=3, refDir=''):
        tf.global_variables_initializer().run()

        print("[*] Reading checkpoint...")
        self.load(self.saver, ckpt_dir)
        self.n_visualize = n_visualize
        self.testData = save_dir.split('/')[-2]
        avg_psnr = []
        avg_ssim = []
        f_acc = open(os.path.join(save_dir[:-7] + '/acc-' + 'ULE' + '.txt'), 'w')

        # for write run time
        f_runtime = open(os.path.join(save_dir[:-7] + '/avg_runtime-' + 'ULE' + '.txt'), 'w')
        run_time = 0

        if post_processing:
            noisy_image = tf.placeholder(tf.float32, [None, None, None, 3])
            _, clean_image = CBDNet(noisy_image)

            var_Denoising = [var for var in tf.trainable_variables() if 'fcn' in var.name] + \
                             [var for var in tf.trainable_variables() if 'unet' in var.name]

            saver_denoising = tf.train.Saver(var_list=var_Denoising)
            saver_denoising.restore(self.sess, 'checkpoint/Denoising/model_%d' % denoising_level)

        print("[*] Testing...")

        for idx, test_low_data_name in enumerate(tqdm(test_low_data_names)):
            # print(test_low_data_name)
            [temp, name] = os.path.split(test_low_data_name)
            [_, dataset] = os.path.split(os.path.split(temp)[0])
            dataset = 'output_' + dataset

            test_low_data = load_images(test_low_data_name)

            if not os.path.exists(os.path.join(save_dir, dataset)):
                os.makedirs(os.path.join(save_dir, dataset))

            suffix = name[name.find('.'):]
            #suffix = '.png'
            name = name[:name.find('.')]

            if test_low_data.shape[-1] == 4:
                test_low_data = cv2.cvtColor(test_low_data, cv2.COLOR_RGBA2RGB)

            input_low_test = np.expand_dims(test_low_data, axis=0)
            start = time.time()
            [t_hat, t, bright, att, S_hat] = self.sess.run([self.t_hat, self.t, self.bright, self.attmap, self.output_S_hat],
                                                    feed_dict={self.input_low: input_low_test})

            if post_processing:
                [S_hat] = self.sess.run([clean_image], feed_dict={noisy_image: S_hat})
            end = time.time()
            run_time += end-start
            avg_run_time = run_time / (idx+1)

            if idx < self.n_visualize:
                save_images(os.path.join(save_dir,'output_ULE_' + name + suffix), S_hat)
                save_images(os.path.join(save_dir, 'input_' + name + suffix), test_low_data)

            if self.testData == 'part2':
                ref_img = get_ref_img(name, refDir)

                out_img = np.squeeze(S_hat)
                out_img = Image.fromarray((out_img * 255).astype(np.uint8), mode='RGB')
                tmp = np.array(out_img)

                avg_psnr.append(get_psnr(tmp, ref_img))
                # print('pnsr:', get_psnr(tmp, ref_img))
                avg_ssim.append(get_ssim(tmp, ref_img))
                # print('ssim:', get_ssim(tmp, ref_img))

        if self.testData == 'part2':
            f_acc.write('avg PSNR: {:.5f}, avg SSIM: {:.5f}'.format(sum(avg_psnr) / len(avg_psnr),
                                                                    sum(avg_ssim) / len(avg_ssim)))
        else:
            f_acc.write("No PSNR, SSIM. Only SICE part2 dataset can calculate PSNR, SSIM.")

        f_runtime.write("avg runtime: {}".format(str(avg_run_time) + 'sec'))
        f_acc.close()
        f_runtime.close()
