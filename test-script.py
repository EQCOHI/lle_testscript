import argparse
import sys
sys.path.append('./code')

from utils import *  # ./code/utils.py
#print('test')

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--model', type=str, required=True, help='select model for LLE 1.zero-dce or 2.ULE..') # if new model added then fix it
    parser.add_argument('--device', type=int, required=True, help='device num, cuda:0')
    parser.add_argument('--testData', type=str, required=True, help='select test img dataset')
    parser.add_argument('--num-visualize', type=int, required=False, default=3, help='Number of Visual Comparison')

    args = parser.parse_args()
    return args


args = parse_args()

# Prepare test dataset path
if args.testData == 'part1':
    testDir= 'data/part1/test-toy'
elif args.testData == 'part2':
    testDir = 'data/part2'
    refDir = 'data/part2_label/'
elif args.testData == 'vv':
    testDir = 'data/vv'

print("\n#################################################")
print("[!] Start test using {} model".format(args.model))
print("[!] Start test using {} dataset".format(args.testData))
print("#################################################\n")

if args.model == 'zero-dce':
    from model.zerodce.predict import predict
    if args.testData == 'part2':
        predict(args.testData, testDir, args.num_visualize, args.device, refDir)
    else:
        refDir = './'
        predict(args.testData, testDir, args.num_visualize, args.device, refDir)
elif args.model == 'retinex':
    from model.RetinexNet.predict import predict

    if args.testData == 'part2':
        predict(args.testData, testDir, args.num_visualize, args.device, refDir)
    else:
        refDir = './'
        predict(args.testData, testDir, args.num_visualize, args.device, refDir)
elif args.model == 'ULE':
    from model.ULE.predict import predict

    if args.testData == 'part2':
        predict(args.testData, testDir, args.num_visualize, args.device, refDir)
    else:
        refDir = './'
        predict(args.testData, testDir, args.num_visualize, args.device, refDir)
